# IntList
By Anton Gubenko

<br>

## Information
This Project is a school Project.  
The Task (in German) is [here](https://gitlab.com/antongub/intlist/blob/master/Task.pdf)

## Specifications
- Our Teacher allowed me to rename the class "IntListe" to "IntList", as well as to write the code in english
- C++ Language standard 98

## Docker build and execute from gitlab registry
Build & Execute: `docker run --rm -it -v /path/to/project:/usr/intlist --name intlist registry.gitlab.com/antongub/intlist:latest`

## Docker build and execute locally
1. Build: `docker build -t intlist:latest .`  
2. Execute: `docker run --rm -it -v /path/to/project:/usr/intlist --name intlist intlist:latest`

## Docker errors
If you get an error message with opening the `start.sh` file it is probably because you're on windows.  
So you firstly need to convert the `start.sh` Newlines CR-LF to Unix Format