/***************************************************************************************
	intelement.h
		declares the IntElement struct

	Anton Gubenko, 2019
***************************************************************************************/

#ifndef INTLIST_INTELEMENT_H
#define INTLIST_INTELEMENT_H

struct IntElement {
    int value;
    IntElement* pNext;
};

#endif
