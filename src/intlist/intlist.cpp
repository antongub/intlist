/***************************************************************************************
	intlist.cpp
		implements the intlist methods

	Anton Gubenko, 2019
***************************************************************************************/

#include <iostream>
#include "intlist.h"


IntList::IntList() {
    mHead = NULL;
}

IntList::~IntList() {
    this->clear();
}


void IntList::clear() {
    IntElement *pCurrent = mHead;
    IntElement *pNext;
    while (pCurrent != NULL) {
        pNext = pCurrent->pNext;
        delete pCurrent;
        pCurrent = pNext;
    }
    mHead = NULL;
}

int IntList::front() {
    if (mHead == NULL)
        return 0;
    return mHead->value;
}

int IntList::back() {
    if (mHead == NULL)
        return 0;
    if (mHead->pNext == NULL)
        return mHead->value;

    IntElement *pCurrent = mHead;
    while (pCurrent->pNext != NULL) {
        pCurrent = pCurrent->pNext;
    }
    return pCurrent->value;
}

int IntList::middle(int position) {
    if (mHead == NULL)
        return 0;

    IntElement *pCurrent = mHead;
    int count = 0;
    while (pCurrent != NULL) {
        if (count == position)
            return (pCurrent->value);
        count++;
        pCurrent = pCurrent->pNext;
    }
    return 0;
}


void IntList::push_back(int value) {
    if (mHead == NULL) {
        mHead = new IntElement();
        mHead->pNext = NULL;
        mHead->value = value;
    } else {
        IntElement *pCurrent = mHead;
        IntElement temp = *mHead;

        while (temp.pNext != NULL) {
            pCurrent = temp.pNext;
            temp = *temp.pNext;
        }

        IntElement *newElement = new IntElement();
        newElement->pNext = NULL;
        newElement->value = value;
        pCurrent->pNext = newElement;
    }
}

void IntList::push_front(int value) {
    if (mHead == NULL) {
        mHead = new IntElement();
        mHead->pNext = NULL;
        mHead->value = value;
    } else {
        IntElement *pNewElement = new IntElement();
        pNewElement->value = value;
        pNewElement->pNext = mHead;
        mHead = pNewElement;
    }
}

void IntList::pop_back() {
    if (mHead == NULL)
        return;
    if (mHead->pNext == NULL) {
        delete mHead;
        mHead = NULL;
        return;
    }

    IntElement *pBack = mHead;
    while (pBack->pNext && pBack->pNext->pNext != NULL) {
        pBack = pBack->pNext;
    }
    delete pBack->pNext;
    pBack->pNext = NULL;
}

void IntList::pop_front() {
    if (mHead == NULL)
        return;
    if (mHead->pNext == NULL) {
        delete mHead;
        mHead = NULL;
        return;
    }

    IntElement *pFront;
    pFront = mHead;
    mHead = mHead->pNext;
    delete pFront;
}


void IntList::insert(int position, int value) {
    IntElement *pNewElement = new IntElement();
    pNewElement->value = value;
    if (position == 0) {
        pNewElement->pNext = mHead;
        mHead = pNewElement;
    } else {
        if(mHead == NULL)
            return;

        IntElement *pCurrentElement;
        int count = 1;
        pCurrentElement = mHead;
        while (count != position) {
            if(pCurrentElement == NULL)
                return;
            pCurrentElement = pCurrentElement->pNext;
            count++;
        }
        pNewElement->pNext = pCurrentElement->pNext;
        pCurrentElement->pNext = pNewElement;
    }
}

void IntList::erase(int position) {
    if (mHead == NULL)
        return;
    IntElement *pTemp = mHead;

    if (position == 0) {
        mHead = pTemp->pNext;
        delete pTemp;
        return;
    }

    for (int i = 0; pTemp != NULL && i < position - 1; i++)
        pTemp = pTemp->pNext;

    if (pTemp == NULL || pTemp->pNext == NULL)
        return;
    IntElement *pNext = pTemp->pNext->pNext;
    delete pTemp->pNext;
    pTemp->pNext = pNext;
}

size_t IntList::size() {
    if (mHead == NULL)
        return 0;

    int size = 1;
    IntElement *pElement = mHead;
    while (pElement->pNext != NULL) {
        pElement = pElement->pNext;
        size++;
    }
    return size;
}
