/***************************************************************************************
	intlist.h
		declares the intlist class

	Anton Gubenko, 2019
***************************************************************************************/

#ifndef INTLIST_INTLIST_H
#define INTLIST_INTLIST_H

#include <cstddef>
#include "intelement.h"

class IntList {
private:
    IntElement* mHead;
public:
    IntList();
    ~IntList();

    void clear();
    int front();
    int back();
    int middle(int position);

    void push_back(int value);
    void push_front(int value);
    void pop_back();
    void pop_front();

    void insert(int position, int value);
    void erase(int position);
    size_t size();
};

#endif
