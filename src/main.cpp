/***************************************************************************************
	main.cpp
		Application Entrypoint

	Anton Gubenko, 2019
***************************************************************************************/

#include <iostream>
#include <cstdio>
#include "./test/init_test.h"

using namespace std;


int main() {
    cout << "IntList by Anton Gubenko!" << endl << endl;

    init_test();

    cout << endl << endl << "To exit the console application enter a character and hit enter.";
    getchar();
    return 0;
}
