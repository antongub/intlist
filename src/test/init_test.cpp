/***************************************************************************************
	init_test.cpp
		 implements the test functions

	Anton Gubenko, 2019
***************************************************************************************/

#include "init_test.h"
#include "../intlist/intlist.h"
#include <cassert>
#include <iostream>

using namespace std;


void init_test() {
    cout << "If there is no 'success' message appearing, it means a Test went wrong." << endl;

    testConstructorIntList();
    testDestructIntList();

    clear();
    front();
    back();
    middle();

    push_back();
    push_front();
    pop_back();
    pop_front();

    insert();
    erase();
    size();

    example();
}

void testConstructorIntList() {
    cout << "test: Constructor" << endl;

    IntList list;
    assert(&list != NULL);
    assert(sizeof(list) == sizeof(IntList));

    cout << "success: Constructor" << endl;
}

void testDestructIntList() {
    cout << "test: destructor" << endl;

    IntList list;
    list.push_back(1);
    const int beforeSize = list.size();
    list.~IntList();
    const int afterSize = list.size();
    assert(beforeSize > afterSize);
    assert(afterSize == 0);

    cout << "success: destructor" << endl;
}

void clear() {
    cout << "test: clear" << endl;

    IntList list;
    list.clear();
    assert(list.size() == 0);

    const int loopLength = 5;
    for (int i = 0; i < loopLength; i++) {
        list.push_back(i);
    }
    assert(list.size() == loopLength);
    list.clear();
    assert(list.size() == 0);

    cout << "success: clear" << endl;
}

void front() {
    cout << "test: front" << endl;

    IntList list;
    assert(list.front() == 0);
    const int loopLength = 5;
    const int firstElement = 3;
    for (int i = firstElement; i < loopLength; i++) {
        list.push_back(i);
        assert(list.front() == firstElement);
    }
    for (int i = firstElement; i < loopLength; i++) {
        list.push_front(i);
        assert(list.front() == i);
    }

    cout << "success: front" << endl;
}

void back() {
    cout << "test: back" << endl;

    IntList list;
    assert(list.back() == 0);
    const int loopLength = 5;
    for (int i = 0; i < loopLength; i++) {
        list.push_back(i);
        assert(list.back() == i);
    }

    cout << "success: back" << endl;
}

void middle() {
    cout << "test: middle" << endl;

    IntList list;
    assert(list.middle(0) == 0);
    const int loopLength = 5;
    const int values[loopLength] = {1, 2, 3, 4, 5};
    for (int i = 0; i < loopLength; i++) {
        list.push_back(values[i]);
        assert(list.middle(i) == values[i]);
    }
    assert(list.middle(loopLength) == 0);

    cout << "success: middle" << endl;
}


void push_back() {
    cout << "test: push_back" << endl;

    IntList list;
    const int loopLength = 3;
    for (int i = 0; i < loopLength; i++) {
        list.push_back(i);
        assert(list.back() == i);
    }
    assert(list.size() == loopLength);

    cout << "success: push_back" << endl;
}

void push_front() {
    cout << "test: push_front" << endl;

    IntList list;
    const int loopLength = 3;
    for (int i = 0; i < loopLength; i++) {
        list.push_front(i);
        assert(list.front() == i);
    }
    assert(list.size() == loopLength);

    cout << "success: push_front" << endl;
}

void pop_back() {
    cout << "test: pop_back" << endl;

    IntList list;
    list.pop_back();
    assert(list.back() == 0);
    const int firstElement = 0;
    const int loopLength = 3;
    for (int i = firstElement; i < loopLength; i++) {
        list.push_front(i);
    }
    for (int i = firstElement; i < loopLength; i++) {
        assert(list.back() == i);
        list.pop_back();
        if (list.size() > 0)
            assert(list.back() == (i + 1));
    }
    list.pop_back();
    assert(list.size() == 0 && list.back() == 0);

    cout << "success: pop_back" << endl;
}

void pop_front() {
    cout << "test: pop_front" << endl;

    IntList list;
    list.pop_front();
    assert(list.front() == 0);
    const int firstElement = 0;
    const int loopLength = 3;
    for (int i = firstElement; i < loopLength; i++) {
        list.push_back(i);
    }
    for (int i = firstElement; i < loopLength; i++) {
        assert(list.front() == i);
        list.pop_front();
        if (list.size() > 0)
            assert(list.front() == (i + 1));
    }
    list.pop_front();
    assert(list.size() == 0 && list.front() == 0);

    cout << "success: pop_front" << endl;
}


void insert() {
    cout << "test: insert" << endl;

    IntList list;
    assert(list.middle(100) == 0);
    const int loopLength = 3;
    int lastListSize;
    for (int i = 0; i < loopLength; i++) {
        lastListSize = list.size();
        list.insert(i, i);
        assert(list.middle(i) == i && lastListSize < list.size());
    }

    cout << "success: insert" << endl;
}

void erase() {
    cout << "test: erase" << endl;

    IntList list;
    list.erase(100);

    const int loopLength = 5;
    for (int i = 0; i < loopLength; i++) {
        list.push_back(i);
    }
    for (int i = loopLength - 1; i > 0; i--) {
        assert(list.middle(i) == i);
        list.erase(i);
        assert(list.middle(i) != i);
    }

    const int lastListSize = list.size();
    list.erase(0);
    assert(lastListSize > list.size());

    cout << "success: erase" << endl;
}

void size() {
    cout << "test: size" << endl;

    IntList list;
    assert(list.size() == 0);
    list.push_back(0);
    assert(list.size() == 1);
    list.push_front(0);
    assert(list.size() == 2);
    list.insert(1, 0);
    assert(list.size() == 3);
    list.pop_front();
    assert(list.size() == 2);
    list.pop_back();
    assert(list.size() == 1);
    list.erase(0);
    assert(list.size() == 0);

    cout << "success: size" << endl;
}

void example() {
    cout << endl << "Example" << endl;

    IntList list;
    list.push_back(17);
    list.push_back(23);
    cout << "List contains now: ";
    for(int i = 0; i < list.size(); i ++) {
        cout << list.middle(i) << " ";
    }

    list.pop_back();
    list.push_front(48);
    cout << endl << "List contains now: ";
    for(int i = 0; i < list.size(); i ++) {
        cout << list.middle(i) << " ";
    }
}