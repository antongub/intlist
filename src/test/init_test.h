/***************************************************************************************
	init_test.h
		declares the test functions

	Anton Gubenko, 2019
***************************************************************************************/

#ifndef INTLIST_INIT_TEST_H
#define INTLIST_INIT_TEST_H

void init_test();

void testConstructorIntList();
void testDestructIntList();

void clear();
void front();
void back();
void middle();

void push_back();
void push_front();
void pop_back();
void pop_front();

void insert();
void erase();
void size();

void example();

#endif
